# Quick-Sort
Program is implemented in C language and Dev-C compiler was used. Only I/0 Library was imported and rest of the program is implemented.

You have to first select pivotal element which will divide the array into 2 parts. (Left & Right)

Now compare the full array with the pivotal element and shift the smaller one to lower array indexes and larger to higher indexes. This has created a breakthrough, and divide-and-conquer split they array into smaller sorted arrays (with respect to pivotal element).
Now divide the main array into smaller arrays and select the pivotal element for each array and do the same as done in the previous step.

Nikola Chittar - Chief Managing Director Pr Relations at <a href="http://99onlinepoker.co">domino online</a>.